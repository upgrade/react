import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button } from 'react-native';
import firebase from 'firebase';

type Props = {};
export default class App extends Component<Props> {

  // constructor(props){
  //   super(props)
  //   this.state ={pontuacao : 0}
  // }

  componentWillMount() {

    var config = {
      apiKey: "AIzaSyBBsLiCIX8I9_WYTw9f9zQLcXkey2vNtzE",
      authDomain: "configuracaofirebasereac-fc516.firebaseapp.com",
      databaseURL: "https://configuracaofirebasereac-fc516.firebaseio.com",
      projectId: "configuracaofirebasereac-fc516",
      storageBucket: "configuracaofirebasereac-fc516.appspot.com",
      messagingSenderId: "267294079117"
    };
    firebase.initializeApp(config);

  }

  // salvarDados(){
  //   var funcionarios = firebase.database().ref("funcionarios");
  //   //funcionarios.child("002").child("nome").set("Clara");
  //   //funcionarios.remove(); remove o no completo
  //   //funcionarios.remove();
  //   //definir a referencia de nó inicial 
  //   // database.ref("pontuacao").set("100"); //salvar e atualizar
  //   //database.ref("pontuacao").remove(); //remover dados

  //   //funcionarios.push().child("nome").set("upgrade"); //define um identificador unico automatico

  //   funcionarios.push().set(
  //     {
  //       nome: "cleison teste",
  //       altura: "1.73"
  //     }
  //   )

  // }

  // listarDados(){
  //   var pontuacao = firebase.database().ref("pontuacao");

  //   //toda vez que houver uma alteração ele envia um listener on
  //   pontuacao.on('value', (snapshot) => {
  //    // alert(snapshot.val())
  //    var pontos = snapshot.val();
  //    this.setState({pontuacao:pontos})
  //   });

  // }

  cadastrarUsuario() {
    var email = "teste@teste.com";
    var senha = "123456";

    //retorna um objeto para manipulat autenticação
    const usuario = firebase.auth();
    usuario.createUserWithEmailAndPassword(
      email,
      senha
    ).catch(
      (erro) => {
        var mensagemErro = "";
        if (erro.code == "auth/weak-password") {
          mensagemErro = "A senha precisa ter no mínimo 6 caracteres";
        }
        //erro.code , erro.message
        alert(mensagemErro)
      }
    );

  }

  verificarUsuarioLogado() {

    var usuario = firebase.auth();

    usuario.onAuthStateChanged(
      (usuarioAtual) => {
        if (usuarioAtual) {
          alert("usuário esta logado");
        }
        else {
          alert("usuario não esta logado")
        }
      }
    );

    // var usuarioAtual = usuario.currentUser;

    // if(usuarioAtual){
    //   alert("usuário esta logado");
    // }
    // else{
    //   alert("usuario não esta logado")
    // }

  }

  deslogarUsuario() {
    var usuario = firebase.auth();
    usuario.signOut();
  }

  logarUsuario(){
    var email = "teste@teste.com";
    var senha = "123456";
    var usuario = firebase.auth();

    usuario.signInWithEmailAndPassword(
      email,
      senha
    ).catch(
      (erro) =>{
        alert(erro.message)
      }
    );


  }

  render() {

    return (

      <View  >
        <Button
          onPress={() => { this.cadastrarUsuario() }}
          title="Cadastrar Usuário"
          color="#841584"
          accessibilityLabel="Cadastrar Usuário"
        />

        <Button
          onPress={() => { this.verificarUsuarioLogado() }}
          title="Verificar Usuário Logado"
          color="#841584"
          accessibilityLabel="Verificar Usuário Logado"
        />

        <Button
          onPress={() => { this.deslogarUsuario() }}
          title="Deslogar Usuário"
          color="#841584"
          accessibilityLabel="Deslogar Usuário"
        />

        <Button
          onPress={() => { this.logarUsuario() }}
          title="Logar Usuário"
          color="#841584"
          accessibilityLabel="Logar Usuário"
        />

        <Text></Text>
      </View>
    );
  }
}

