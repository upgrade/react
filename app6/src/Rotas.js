import React, { Component } from 'react';
import { Platform, StyleSheet, Image, Text, View } from 'react-native';
//adicionado a stack pois a nova versão exite isso como se fosse a view para 
//englobar os componentes restantes
import { Router, Scene, Stack } from 'react-native-router-flux';

import Principal from './components/Principal';
import SobreJogo from './components/SobreJogo';
import OutrosJogos from './components/OutrosJogos';
import Resultado from './components/Resultado';

const rotas = () => (
    <Router >
        <Stack key='root'>
            <Scene key='principal' component={Principal} initil title="Cara ou Coroa" />
            <Scene key='sobreJogo' component={SobreJogo} />
            <Scene key='outrosJogos' component={OutrosJogos} />
            <Scene key='resultado' component={Resultado} />
        </Stack>
    </Router>
)

export default rotas;
