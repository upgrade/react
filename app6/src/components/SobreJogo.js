import React, { Component } from 'react';
import {Text, StyleSheet} from 'react-native';

export default class SobreJogo extends Component{

    render(){
        return(
            <Text style={styles.textoJogo}>
            Aqui Podem ser apresentadas informações sobre o jogo
            </Text>
        )
    }
}

const styles = StyleSheet.create({
    textoJogo:{
        flex: 1,
        backgroundColor: '#61bd8c'
    }
})