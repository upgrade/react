

import React, { Component } from 'react';
import { Platform, StyleSheet, TouchableHighlight, Image, Text, View } from 'react-native';

import { Actions } from 'react-native-router-flux';

const logo = require('../imgs/logo.png');
const btnJogar = require('../imgs/botao_jogar.png');
const btnSobreJogo = require('../imgs/sobre_jogo.png');
const btnOutrosJogos = require('../imgs/outros_jogos.png');

type Props = {};
export default class Principal extends Component<Props> {
    render() {
        return (
            <View style={styles.cenaPrincipal}>

                <View style={styles.apresentacaoJogo} >
                    <Image source={logo}></Image>
                    <TouchableHighlight 
                         onPress={() => {
                            Actions.resultado()
                        }}
                    >
                         <Image source={btnJogar}></Image>
                    </TouchableHighlight>
                </View>

                <View style={styles.rodape}>
                    <TouchableHighlight
                        onPress={() => {
                            Actions.sobreJogo()
                        }}
                    >
                        <Image source={btnSobreJogo}></Image>
                    </TouchableHighlight>

                    <TouchableHighlight
                        onPress={() => {
                            Actions.outrosJogos()
                        }}
                    >
                        <Image source={btnOutrosJogos}></Image>
                    </TouchableHighlight>
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    cenaPrincipal: {
        flex: 1,
        backgroundColor: '#61bd8c'
    },
    apresentacaoJogo: {
        flex: 10,
        //centro na vertical
        justifyContent: 'center',
        //center na horizontal
        alignItems: 'center'
    },
    rodape: {
        //ocupar 1 parte
        flex: 1,
        //deixar os botões em linha
        flexDirection: 'row',
        //espaço entre as imagens
        justifyContent: 'space-between'
    }

})