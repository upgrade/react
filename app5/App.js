
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, StatusBar } from 'react-native';
import { Navigator } from 'react-native-deprecated-custom-components'

import CenaPrincipal from './src/components/CenaPrincipal';
import CenaClientes from './src/components/CenaClientes';
import CenaContatos from './src/components/CenaContatos';
import CenaEmpresa from './src/components/CenaEmpresa';
import CenaServico from './src/components/CenaServicos';


type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      //<CenaPrincipal />
      <Navigator
        //qual a rota inicial
        initialRoute={{ id: 'principal' }}

        //rota em sim, função de callback, definir a cena com base na rota
        renderScene={(route, navigator) => {

          switch(route.id){
            case 'principal' : return (<CenaPrincipal navigator={navigator} />);

            case 'cliente' :  return (<CenaClientes navigator={navigator} />);

            case 'contato': return (<CenaContatos navigator={navigator} />);

            case 'empresa' : return (<CenaEmpresa navigator={navigator} />);

            case 'servico' :  return (<CenaServico navigator={navigator} />);

            default : 
            return false;
          }

        }}
      />
    );
  }
}

