
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, StatusBar, Image } from 'react-native';


//importar o componente de barra de navegação
import BarraNavegacao from './BarraNavegacao'

const detalheEmpresa = require('../imgs/detalhe_empresa.png');


type Props = {};
export default class CenaEmpresa extends Component<Props> {
    render() {
        return (
            <View style={{flex: 1, backgroundColor: '#fff'}}>
                <StatusBar
                    //hidden //true ou false
                    backgroundColor='#ec7148'
                />
                {/* inserido o navigator para que na barra de navegação pega essa props e consiga voltar a cena anterior ou alguma em especifico */}
                <BarraNavegacao voltar navigator={this.props.navigator} corDeFundo='#ec7148'/>

                <View style={styles.cabecalho}>
                    <Image source={detalheEmpresa}></Image>
                    <Text style={styles.txtTitulo}>A Empresa</Text>
                </View>  
                <View style={styles.detalheEmpresa}>  
                   <Text style={styles.txtEmpresa}>Lorem ipsum dolor sit amet, consectetur 
                   adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut </Text>
                </View>

              
            </View>

        );
    }
}
const styles = StyleSheet.create({
    cabecalho:{
        flexDirection: 'row',
        marginTop: 20
    },
    txtTitulo:{
        fontSize: 30,
        color: '#ec7148',
        marginLeft: 10,
        marginTop: 25
    },
    detalheEmpresa:{
        marginTop: 20,
        padding: 20
    },
    txtEmpresa:{
        fontSize: 18
    }
});

