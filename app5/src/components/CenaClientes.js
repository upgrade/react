
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, StatusBar, Image } from 'react-native';


//importar o componente de barra de navegação
import BarraNavegacao from './BarraNavegacao'

const detalheClientes = require('../imgs/detalhe_cliente.png');
const cliente1 = require('../imgs/cliente1.png');
const cliente2 = require('../imgs/cliente2.png');


type Props = {};
export default class CenaClientes extends Component<Props> {
    render() {
        return (
            <View style={{flex: 1, backgroundColor: '#fff'}}>
                <StatusBar
                    //hidden //true ou false
                    backgroundColor='#b9c941'
                />
                {/* inserido o navigator para que na barra de navegação pega essa props e consiga voltar a cena anterior ou alguma em especifico */}
                <BarraNavegacao voltar navigator={this.props.navigator} corDeFundo='#b9c941'/>

                <View style={styles.cabecalho}>
                    <Image source={detalheClientes}></Image>
                    <Text style={styles.txtTitulo}>Nossos Cliente</Text>
                </View>  
                <View style={styles.detalheCliente}>  
                    <Image source={cliente1}></Image>
                    <Text style={styles.txtDetalheCliente}>Loren ipsum</Text>
                </View>

                 <View style={styles.detalheCliente}>    
                    <Image source={cliente2}></Image>
                    <Text style={styles.txtDetalheCliente}>Loren ipsum</Text>
                 </View>   

            </View>

        );
    }
}
const styles = StyleSheet.create({
    cabecalho:{
        flexDirection: 'row',
        marginTop: 20
    },
    txtTitulo:{
        fontSize: 30,
        color: '#b9c941',
        marginLeft: 10,
        marginTop: 25
    },
    detalheCliente:{
        padding: 20,
        marginTop:10
    },
    txtDetalheCliente:{
        fontSize:18,
        marginLeft: 20
    }
});

