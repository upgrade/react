
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, StatusBar, Image } from 'react-native';


//importar o componente de barra de navegação
import BarraNavegacao from './BarraNavegacao'

const detalheContatos = require('../imgs/detalhe_contato.png');


type Props = {};
export default class CenaContatos extends Component<Props> {
    render() {
        return (
            <View style={{flex: 1, backgroundColor: '#fff'}}>
                <StatusBar
                    //hidden //true ou false
                    backgroundColor='#61bd8c'
                />
                {/* inserido o navigator para que na barra de navegação pega essa props e consiga voltar a cena anterior ou alguma em especifico */}
                <BarraNavegacao voltar navigator={this.props.navigator} corDeFundo='#61bd8c'/>

                <View style={styles.cabecalho}>
                    <Image source={detalheContatos}></Image>
                    <Text style={styles.txtTitulo}>Contatos</Text>
                </View>  
                <View style={styles.detalheContatos}>  
                   <Text style={styles.txtContatos}>Tel: (99) 9999999</Text>
                   <Text style={styles.txtContatos}>Cel: (99)999999</Text>
                   <Text style={styles.txtContatos}>E-mail: teste@teste.com</Text>
                </View>

              
            </View>

        );
    }
}
const styles = StyleSheet.create({
    cabecalho:{
        flexDirection: 'row',
        marginTop: 20
    },
    txtTitulo:{
        fontSize: 30,
        color: '#61bd8c',
        marginLeft: 10,
        marginTop: 25
    },
    detalheContatos:{
        marginTop: 20,
        padding: 20
    },
    txtContatos:{
        fontSize: 18
    }
});

