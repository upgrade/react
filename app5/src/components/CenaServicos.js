
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, StatusBar, Image } from 'react-native';


//importar o componente de barra de navegação
import BarraNavegacao from './BarraNavegacao'

const detalheServico = require('../imgs/detalhe_servico.png');


type Props = {};
export default class CenaServicos extends Component<Props> {
    render() {
        return (
            <View style={{flex: 1, backgroundColor: '#fff'}}>
                <StatusBar
                    //hidden //true ou false
                    backgroundColor='#19d1c8'
                />
                {/* inserido o navigator para que na barra de navegação pega essa props e consiga voltar a cena anterior ou alguma em especifico */}
                <BarraNavegacao voltar navigator={this.props.navigator} corDeFundo='#19d1c8'/>

                <View style={styles.cabecalho}>
                    <Image source={detalheServico}></Image>
                    <Text style={styles.txtTitulo}>Nossos Serviços</Text>
                </View>  
                <View style={styles.detalheServico}>  
                   <Text style={styles.txtServico}>-Consultoria</Text>
                   <Text style={styles.txtServico}>-Processos</Text>
                   <Text style={styles.txtServico}>-Acompanhamento de Projetos</Text>
                </View>

              
            </View>

        );
    }
}
const styles = StyleSheet.create({
    cabecalho:{
        flexDirection: 'row',
        marginTop: 20
    },
    txtTitulo:{
        fontSize: 30,
        color: '#19d1c8',
        marginLeft: 10,
        marginTop: 25
    },
    detalheServico:{
        marginTop: 20,
        padding: 20
    },
    txtServico:{
        fontSize: 18
    }
});

