import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image } from 'react-native';

type Props = {};
export default class Itens extends Component<Props> {

    // //acontece primeiro ciclo de vida
    // constructor(props){
    //     super(props);
    //     console.log("construindo a aplicação");

    // }

    // //segundo
    // componentWillMount(){
    //     console.log("fazer alguma coisa antes do render");

    // }

    // //quarto
    // componentDidMount(){
    //     console.log("fazer alguma coisa depois do render");
    // }

    //terceiro 
    render() {
        console.log("render");
        return (
            <View style={styles.item}>
                <View style={styles.foto}>
                    <Image  style={{height: 100, width: 100}} source={{ uri: this.props.item.foto } } />
                </View>

                <View style={styles.detalhesItens}>
                    <Text style={styles.txtTitulo}>{this.props.item.titulo}</Text>
                    <Text style={styles.txtValor}>R$ {this.props.item.valor}</Text>
                    <Text>Local: {this.props.item.local_anuncio}</Text>
                    <Text>Dt Publicação: {this.props.item.data_publicacao}</Text>
                </View>
               
            </View>
        );
    }
}

const styles = StyleSheet.create({
    item:{
        borderWidth: 0.5,
        borderColor: '#999',
        margin: 10,
        padding: 10,
        flexDirection: 'row',
    },
    foto:{
        width: 102,
        height:102
    },
    detalhesItens:{
        marginLeft: 20,
        flex: 1
    },
    txtTitulo:{
        fontSize: 18,
        color: 'blue',
        marginBottom: 5
    },
    txtValor:{
        fontSize: 16,
        fontWeight: 'bold'
    }
});

