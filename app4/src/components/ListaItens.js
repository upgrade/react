import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, ScrollView } from 'react-native';
import Itens from './Itens'
import axios from 'axios'

type Props = {};
export default class ListaItens extends Component<Props> {

    // //acontece primeiro ciclo de vida
    // constructor(props){
    //     super(props);
    //     console.log("construindo a aplicação");

    // }

    // //segundo
    // componentWillMount(){
    //     console.log("fazer alguma coisa antes do render");

    // }

    // //quarto
    // componentDidMount(){
    //     console.log("fazer alguma coisa depois do render");
    // }

    constructor(props){
        super(props);
        this.state = {listaItens: []}
    }

    componentWillMount(){
        //requisição http
        // url de teste JSON http://faus.com.br/recursos/c/dmairr/api/itens.html
        axios.get('http://faus.com.br/recursos/c/dmairr/api/itens.html')
            //uma resposta, promessa
            .then( response => {
                this.setState({listaItens: response.data});
            })
            //erro da resposta
            .catch(() => {console.log('Erro ao recuperar os dados');
            })
    }

    //terceiro 
    render() {
        return (
            <ScrollView>
                {/* para cada interação da lista o map atua para interar nela tendo uma função callback */}
                { this.state.listaItens.map(item => (<Itens key={item.titulo} item={item} />)) }
               
            </ScrollView>
        );
    }
}


