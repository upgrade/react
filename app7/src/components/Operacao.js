import React, { Component } from 'react';
import { Picker, StyleSheet } from 'react-native';

export default class Operacao extends Component {

    // constructor(props){
    //     super(props);
    //     //inicia o estado da operação que vai escolher
    //     this.state = {operacao: ''};
    // }

    render() {
        return (
            //os  atributos do picker são o primero estilo, segundo o estado da operação e o terceiro pega a operacao e armazena no estado
            <Picker style={styles.operacao}
                selectedValue={this.props.operacao}
                onValueChange={op => { this.props.atualizaOperacao(op) }}
            >
                <Picker.Item label='Soma' value='soma' />
                <Picker.Item label='Subtração' value='subtracao' />
            </Picker>
        )
    }
}


const styles = StyleSheet.create({
    operacao: {
        marginTop: 15,
        marginBottom: 15
    }
})