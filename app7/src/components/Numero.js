import React from 'react';
import { TextInput, View, StyleSheet } from 'react-native';

//input recebe o props onde vai passar de cara numero na classe entrada
export default props =>(
    <TextInput 
        style={styles.numero} 
        value={props.num} 
        placeholder="Numero"
        onChangeText={valorDoCampo => props.atualizaValor(props.nome, valorDoCampo) }
        />
)

const styles = StyleSheet.create({
    numero:{
        width: 140,
        height: 80,
        fontSize: 20
    }
})