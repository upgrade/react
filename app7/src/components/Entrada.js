import React from 'react';
import { Text, View, StyleSheet } from 'react-native';

import Numero from './Numero'

export default props => (
    
    <View style={styles.numeros}>
        {/* o num vem da classe numero onde pega o valor de cada input e coloca no numero, e o num1 2 num 2 vão se levados
        ate o painel */}
        <Numero num={ props.num1 } atualizaValor={props.atualizaValor} nome='num1' />
        <Numero num={ props.num2 } atualizaValor={props.atualizaValor} nome='num2' />
    </View>
   
)

const styles = StyleSheet.create({
    numeros: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    }
})