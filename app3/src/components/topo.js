import React, { Component } from 'react';
import { Image, View } from 'react-native';

//boa pratica separar o require
const imagem = require('../../imgs/jokenpo.png');

//uma separação por componente, modularização
class Topo extends Component {
    render() {
        return (
            <View>
                <Image source={imagem} />
            </View>
        )
    }
}
export default Topo;