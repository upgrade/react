/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, {Component} from 'react';
import { Button, StyleSheet, Text, View} from 'react-native';
import Topo from './src/components/topo';
import Icone from './src/components/icone';

// class MeuComponente extends Component{

//   render(){
//     return(
//       <View>
//         <Text>{this.props.teste}</Text>
//       </View>
//     )
//   }

// }


type Props = {};
export default class App extends Component<Props> {
  
  // constructor(props){
  //   super(props);
  //   this.state = {texto : 'Texto Teste 2'};
  // }

  // alteraTexto(){
  //   this.setState({texto : 'Outra Coisa'});
  // }

  constructor(props){
    super(props);
    //estados que estão sendo carregados vazios para que assim so ao clique 
    //do botão seja executado a ação de escolha de papel, pedra ou tesoura
    this.state = {escolhaUsuario: '', escolhaComputador: '', resultado: ''}
  }

  jokenpo(escolhaUsuario){

    //gerar numero aleatorio(0, 1 ,2)
    var numAleatorio = Math.floor(Math.random() * 3);

    //carregar vazio
    var escolhaComputador = '';

    //substituindo o numero aleatorio pela escolha
    switch(numAleatorio){
      case 0: escolhaComputador = 'pedra'; break;
      case 1: escolhaComputador = 'papel'; break;
      case 2: escolhaComputador = 'tesoura'; break;
    }

    var resultado = '';

    if(escolhaComputador == 'pedra'){
      if(escolhaUsuario == 'pedra'){
        resultado = 'Empate';
      }
      if(escolhaUsuario == 'papel'){
        resultado = 'Você Ganhou';
      }
      if(escolhaUsuario == 'tesoura'){
        resultado = 'Você Perdeu';
      }
    }

    if(escolhaComputador == 'papel'){
      if(escolhaUsuario == 'papel'){
        resultado = 'Empate';
      }
      if(escolhaUsuario == 'tesoura'){
        resultado = 'Você Ganhou';
      }
      if(escolhaUsuario == 'pedra'){
        resultado = 'Você Perdeu';
      }
    }

    if(escolhaComputador == 'tesoura'){
      if(escolhaUsuario == 'tesoura'){
        resultado = 'Empate';
      }
      if(escolhaUsuario == 'pedra'){
        resultado = 'Você Ganhou';
      }
      if(escolhaUsuario == 'papel'){
        resultado = 'Você Perdeu';
      }
    }

    //passa a mudança do estado conforme vai clicando no botão e feito as logicas
    this.setState({ escolhaUsuario : escolhaUsuario,
                    escolhaComputador : escolhaComputador,
                    resultado: resultado
    }) 
  }

  render() {
    return (
     <View> 

      <Topo></Topo>

      <View style={styles.painelAcoes}>
        <View style={styles.btnEscolha}>
          <Button title="pedra" onPress={ () => { this.jokenpo('pedra') } } />
        </View>
        <View style={styles.btnEscolha}>
          <Button title="papel" onPress={ () => { this.jokenpo('papel') } } />
        </View>
        <View style={styles.btnEscolha}>
          <Button title="tesoura" onPress={ () => { this.jokenpo('tesoura') } } />
        </View>
      </View>

      <View style={styles.palco}>
        <Text style={styles.txtResultado}>{this.state.resultado}</Text>

        <Icone escolha={this.state.escolhaComputador} jogador={'Computador'} ></Icone>
        <Icone escolha={this.state.escolhaUsuario} jogador={'Você'} ></Icone>
        
        
      </View>
   
     {/* <MeuComponente teste={this.state.texto}>
     </MeuComponente>
     <Button 
      onPress={() => {this.alteraTexto()}}
      title='Botão'
      />  */}

     
      
     

     </View>
    );
  }
}





//estilo do layout
const styles = StyleSheet.create({
  btnEscolha:{
    width: 90,
  },
  painelAcoes:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10
  },
  palco:{
    alignItems: 'center',
    marginTop: 10
  },
  txtResultado:{
    fontSize: 25,
    fontWeight: 'bold',
    color: 'red',
    height: 60
  },
  icone:{
    alignItems: 'center',
    marginBottom: 20,
    
  },
  txtJogador:{
    fontSize: 18
  }
});
