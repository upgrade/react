/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, {Component} from 'react';
import {Platform, Text, View, Button} from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

const geraNumeroAletorio = () =>{
  var numero_aleatorio = Math.random();
  numero_aleatorio = Math.floor((numero_aleatorio * 10));
  alert(numero_aleatorio);
}

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
     <View>
       <Text>nnmbn</Text>
       <Button 
        title="Gerar Um Número Randonico"
        onPress={geraNumeroAletorio}
       />
     </View>
    );
  }
}
