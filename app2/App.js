/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, {Component} from 'react';
import {View, Text, Image, TouchableOpacity, Alert} from 'react-native';

//formatações style
const Estilos = {
  principal:{
   flex: 1,
   justifyContent: 'center',
   alignItems: 'center'
  },
  botao:{
    backgroundColor: '#538530',
    paddingVertical: 10,
    paddingHorizontal: 40,
    marginTop: 20
  },

  textoBotao:{
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
  }

 

  // botao:{
  //   backgroundColor: '#48bbec',
  //   padding: 10,
  //   borderColor: '#1d8eb8',
  //   borderWidth: 1,
  //   borderRadius: 8,
  //   shadowColor: '#000',
  //   shadowOffset: {width: 0, height: 2},
  //   shadowOfacity: 0.4
  // },

  // textoBotao:{
  //   color: '#fff',
  //   fontSize: 16,
  //   fontWeight: 'bold',
  //   alignSelf: 'center'
  // }

};

const gerarNovaFrase = () => {

  var numeroAleatorio = Math.random();
  numeroAleatorio = Math.floor(numeroAleatorio * 5);

  //frases
  var frases = Array();
  frases[0] = 'Não perca a motivação só porque as coisas não estão correndo como o previsto. Adversidade gera sabedoria e é isso que levará você ao sucesso.';
  frases[1] = 'Encare o que fez de errado com motivação, pois é isso que o ajudará a fazer certo da próxima vez.';
  frases[2] = 'As pessoas dizem frequentemente que a motivação não dura. Bem, nem o banho - e é por isso que ele é recomendado diariamente.';
  frases[3] = 'Toda ação humana, quer se torne positiva ou negativa, precisa depender de motivação.';
  frases[4] = 'A vida tanto lhe pode dar o melhor como o pior, mas é você quem escolhe aquilo que vai permanecer ou ficar para trás.';

  var fraseEscolhida = frases[numeroAleatorio];

  Alert.alert(fraseEscolhida);
  

}

type Props = {};
export default class App extends Component<Props> {

  render() {
    
    const {principal, botao, textoBotao} = Estilos;
    
    return (
     <View style={principal}>
        <Image source={require('./imgs/logo.png')} />
        <TouchableOpacity 
        onPress={gerarNovaFrase}
        style={botao}>
          <Text style={textoBotao}>Nova Frase</Text>
        </TouchableOpacity>
     </View>
    );
  }
}