import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';

import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reducers from './reducers'
import firebase from 'firebase';
import ReduxThunk from 'redux-thunk';

import Routes from './Routes'


class App2 extends Component {

    componentWillMount(){
        let config = {
            apiKey: "AIzaSyAjdajaHYuiZ0Myo-33agUc0nKY63uXscc",
            authDomain: "whatsapp-clone-b09fc.firebaseapp.com",
            databaseURL: "https://whatsapp-clone-b09fc.firebaseio.com",
            projectId: "whatsapp-clone-b09fc",
            storageBucket: "whatsapp-clone-b09fc.appspot.com",
            messagingSenderId: "873852875589"
          };
          firebase.initializeApp(config);
    }

    render() {
        return (
            <Provider store={createStore(reducers, {}, applyMiddleware(ReduxThunk))} >
                <Routes />
            </Provider>
        )
    }
}

export default App2;