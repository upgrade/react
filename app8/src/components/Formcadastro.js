import React, { Component } from 'react';
import { Platform, StyleSheet, Button, TextInput, Text, View, ImageBackground } from 'react-native';

import { connect } from 'react-redux';

import { modificaEmail, modificaSenha, modificaNome, cadastraUsuario } from '../actions/AutenticacaoActions';

class formCadastro extends Component {

    _cadastraUsuario(){

        const {nome, email, senha} = this.props;

       this.props.cadastraUsuario({nome, email, senha});
    }

    render() {
        return (
            <ImageBackground style={{ flex: 1 }} source={require('../imgs/bg.png')}>
                <View style={styles.corpo}>
                    <View style={styles.form}>
                        <TextInput
                            value={this.props.nome}
                            style={styles.txtInput}
                            placeholder="Nome"
                            onChangeText={texto => this.props.modificaNome(texto)}
                            placeholderTextColor='#fff'
                        />
                        <TextInput
                            value={this.props.email}
                            style={styles.txtInput}
                            placeholder="Email"
                            onChangeText={texto => this.props.modificaEmail(texto)}
                            placeholderTextColor='#fff'
                        />
                        <TextInput
                            value={this.props.senha}
                            style={styles.txtInput}
                            placeholder="Senha"
                            onChangeText={texto => this.props.modificaSenha(texto)}
                            secureTextEntry={true}
                            placeholderTextColor='#fff'
                        />
                        <Text style={{color: '#ff0000', fontSize: 18}} >{this.props.erroCadastro}</Text>
                    </View>

                    <View style={styles.botao}>
                        <Button color="#115e54" title="Cadastrar" onPress={() => this._cadastraUsuario()} />
                    </View>
                </View>
            </ImageBackground>
        )
    }

}

const mapStateToProps = state => {
    console.log(state);

    return (
        {
            nome: state.AutenticacaoReducer.nome,
            email: state.AutenticacaoReducer.email,
            senha: state.AutenticacaoReducer.senha,
            erroCadastro: state.AutenticacaoReducer.erroCadastro,
        }
    )
}

export default connect(mapStateToProps, { modificaEmail, modificaSenha, modificaNome, cadastraUsuario })(formCadastro);

const styles = StyleSheet.create({

    corpo: {
        flex: 1,
        padding: 10
    },
    form: {
        flex: 4,
        justifyContent: 'center'
    },

    botao: {
        flex: 1
    },
    txtInput: {
        fontSize: 20,
        height: 45
    }



})