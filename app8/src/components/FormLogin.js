import React, { Component } from 'react';
import { Platform, StyleSheet, Button, TextInput, Text, View, TouchableHighlight, ImageBackground } from 'react-native';

import { modificaEmail, modificaSenha, autenticarUsuario } from '../actions/AutenticacaoActions';

//action para pegar a chave da rota no route
import { Actions } from 'react-native-router-flux'

//recuperar os estados dos campos
import { connect } from 'react-redux';

class formLogin extends Component {

    _autenticarUsuario(){
        const {email, senha} = this.props;

        this.props.autenticarUsuario({email,senha});

    }

    render() {
        return (
            <ImageBackground style={{ flex: 1 }} source={require('../imgs/bg.png')}>
                <View style={styles.corpo}>

                    <View style={styles.titulo}>
                        <Text style={styles.txtTitulo}>Whats app clone</Text>
                    </View>

                    <View style={styles.corpoLogin}>
                        <TextInput
                            value={this.props.email}
                            style={styles.txtInput}
                            placeholder="Email"
                            onChangeText={texto => this.props.modificaEmail(texto)}
                            placeholderTextColor='#fff'
                        />
                        <TextInput
                            value={this.props.senha}
                            style={styles.txtInput}
                            placeholder="Senha"
                            onChangeText={texto => this.props.modificaSenha(texto)}
                            secureTextEntry={true}
                            placeholderTextColor='#fff'
                        />
                        <TouchableHighlight onPress={() => Actions.formCadastro()}>
                            <Text style={styles.txtLink} >Ainda não tem cadastro? Cadastre-se</Text>
                        </TouchableHighlight>
                    </View>

                    <View style={styles.botao} >
                        <Button color='#115e54' title="Acessar" onPress={() => this._autenticarUsuario()} />
                    </View>
                </View>
            </ImageBackground >
        )
    }
}

const mapStateToProps = state => (
    {
        email: state.AutenticacaoReducer.email,
        senha: state.AutenticacaoReducer.senha
    }
);

export default connect(mapStateToProps, { modificaEmail, modificaSenha, autenticarUsuario })(formLogin);

const styles = StyleSheet.create({

    corpo: {
        flex: 1,
        padding: 10
    },

    titulo: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    txtTitulo: {
        fontSize: 25,
        color: '#fff'

    },

    corpoLogin: {
        flex: 2
    },

    botao: {
        flex: 2
    },

    botaoComponent: {
        color: '#115e54'
    },

    txtInput: {
        fontSize: 20,
        height: 45
    },
    txtLink: {
        fontSize: 20,
        color: '#fff'
    }


})