import {combineReducers} from 'redux';

import AutenticacaoReducer  from './AutenticacaoReducer';

export default combineReducers({
    //função pura do estado da aplicação
    AutenticacaoReducer: AutenticacaoReducer
});